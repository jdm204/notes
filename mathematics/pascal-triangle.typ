#set page(flipped: true, margin: 1cm)
#set text(size: 14pt)
#set align(center)

#let tri-n(n) = range(n + 1).sum()

#let coordinates(horizontalindex, width) = {
    let row = calc.div-euclid(horizontalindex, width)
    let col = calc.rem-euclid(horizontalindex, width)
    (row, col)
}

#let pascalentry(idx, width) = {
    let (row, col) = coordinates(idx, width)
    if calc.odd(row + col) { return " " }

    let mididx = calc.div-euclid(width, 2)
    let centeredcol = col - mididx
    let offsetcol = centeredcol + row
    if offsetcol < 0 or offsetcol > 2 * row {
        return str(" ")
    }
    let bn = calc.binom(row, calc.div-euclid(centeredcol + row, 2))
    str(bn)
}

#let infinitymarks(length) = {
    let marks = (sym.dots.up, sym.dots.down)
    for i in range(calc.div-euclid(length, 2) - 2) {
        marks.insert(1, " ")
    }
    marks.intersperse(sym.dots.v)
}

#let pascaltriangle(trirows) = {
    let gridcols = 2 * (trirows) - 1
    let extrarowlen = gridcols + 4
    grid(
        columns: extrarowlen,
        gutter: 30pt,
        ..range(tri-n(extrarowlen - 2)).map(n => str(pascalentry(n, extrarowlen)))
        + infinitymarks(extrarowlen).map(str)
    )
}

#pascaltriangle(11)
#v(3em)
= Pascal's Triangle


