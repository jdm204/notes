#import "mathematics-1.typ": *

#show: doc => conf(
  title: [
    The Kolmogorov Axioms
  ],
    subtitle: [a foundation for probability],
  doc,
)

Let $(Omega, F, P)$ be a *measure space* with $P(E)$ being the probability of an event $E$, and unlike measure spaces in general, $P(Omega) = 1$.
This makes $(Omega, F, P)$ a *probability space*, comprised of a *sample space* $Omega$, an *event space* $F$, and a *probability measure* $P$.

The components of a *probability space* have interperetations that those of a general *measure space* do not.
Consider an experiment, each realisation of which produces a single *outcome*.
The set of all possible outcomes is $Omega$. $F$ is a set of *events*---an event $E in F$ occurs when an outcome $omega in Omega$ is also an element of $E$.
Many such events can occur for a given outcome.

#line(length: 100%)

#grid(
  columns: (1fr, 1fr),
    [
        #axiom(1)[First Axiom][non-negativity][$ forall E in F, P(E) in RR and P(E) gt.eq 0 $]
        #axiom(2)[Second Axiom][unit measure][$ P(Omega) = 1 $]
    ],
    [
        #axiom(3)[Third Axiom][$sigma$-additivity][
            For a *countable* sequence of *disjoint* sets $E_1, E_2, E_3, ...$ (*mutually exclusive events* imply *disjoint sets*)\
            $ P(union_(i = 1)^(oo) E_i) = sum_(i = 1)^(oo) P(E_i) $
        ]
    ],
)
#line(length: 100%)
= Consequences
#v(20pt)
#columns(2)[
    #set align(left)
    
    #consequence(1)[Probability of the Empty Set][$ P(emptyset) = 0 $][
        As $emptyset union emptyset = emptyset$, we have that $P(emptyset union emptyset) = P(emptyset)$.
    
        $P(emptyset) + P(emptyset) = P(emptyset)$ by the previous and #ref[A3].
    
        Subtracting $P(emptyset)$ from both sides yields the result $P(emptyset) = 0$.
    ]

    #consequence(2)[Monotonicity][$ A subset.eq B => P(A) lt.eq P(B) $][
        Let $A "and" B$ be arbitrary sets with $A subset.eq B$.
    
        Let $E_i$ be the sequence with elements $E_1 = A$, $E_2 = B without A$, $E_i = emptyset "for" i gt.eq 3$.
        This ensures the sets $E_i$ are pairwise disjoint and $union_i^oo E_i = B$.

        Thus by #ref[A3] and #ref[C1] we have that $P(B) = P(A) + P(B without A) + sum_(i=3)^oo 0$.

        Since by #ref[A1] all terms on the right hand side are non-negative and $P(B)$ is finite, we have $P(A) lt.eq P(B)$.
    ]

    #consequence(3)[The Complement Rule][$ P(A^c) = 1 - P(A) $][
        We know $A$ and $A^c$ are disjoint/mutually exclusive and $A union A^c = Omega$.
        
        By #ref[A3], and #ref[A2], $P(A union A^c) = P(A) + P(A^c) = P(Omega) = 1$.
        
        Thus, $P(A^c) = 1 - P(A)$.
    ]

    #consequence(4)[The Sum Rule][$ P(A union B) = P(A) + P(B) - P(A sect B) $][
        From #ref[A3], $P(A union B) = P(A) + P(B without A)$.
        
        So, $P(A union B) = P(A) + P(B without (A sect B))$.
        
        We also have $P(B) = P(B without (A sect B))+ P(A sect B)$.

        Eliminating $P(B without (A sect B))$ from both equations yields the result.
    ]

 ]
