#let purple = rgb("b304cb")
#let darkpurple1 = rgb("3E0C87")
#let offwhite = rgb("fffffe")

#let ref(r) = {text(purple)[#r]}

#let axiom(n, name, description, statement) = {
    align(center)[= #text(purple)[A#n] | #name]
    emph[#description]
    text[\ ]
    statement
}

#let consequence(n, name, statement, proof) = {
    align(left)[== #text(purple)[C#n] | #name]
    statement

    par[_Proof._]

    par[#proof $qed$]
    v(8pt)
}

#let conf(
  title: none,
  subtitle: [],
  doc,
) = {
    set page(paper: "a4",
         margin: (x: 42pt, y: 36pt),
        header: align(right)[#text(offwhite)[Dr JD Matthews]])
    set align(center)
    text(darkpurple1, 22pt, weight: "bold")[
        #title
    ]
    linebreak()
    linebreak()
    
    text(18pt)[
        #emph[#subtitle]
    ]

    linebreak()
    linebreak()

    line(length: 100%)

    doc
}
