% Created 2023-05-21 Sun 15:19
% Intended LaTeX compiler: lualatex
\documentclass[11pt]{article}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\directlua{luaotfload.add_fallback
("seriffallback",
{
"NotoSerif:mode=harf;"
}
)}
\directlua{luaotfload.add_fallback
("sansfallback",
{
"NotoSans:mode=harf;"
}
)}
\directlua{luaotfload.add_fallback
("emojifallback",
{
"NotoColorEmoji:mode=harf;"
}
)}
\usepackage{microtype}
\usepackage{luaotfload}
\usepackage{amssymb,amsmath}
\usepackage{verbatim}
\usepackage[margin=3cm]{geometry}
\usepackage{color}
\usepackage{placeins}
\usepackage{parskip}%
\usepackage{float}
\usepackage{floatpag}
\usepackage{unicode-math}
\usepackage{setspace}
\usepackage{graphicx}
\usepackage{svg}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usetikzlibrary{intersections}
\usepackage{pgfplots}
\pgfplotsset{compat = newest}
\setcounter{secnumdepth}{0}
\setlength{\parindent}{0pt}
\setmainfont[RawFeature={fallback=seriffallback}]{TeX Gyre Pagella}
\setsansfont[RawFeature={fallback=emojifallback}]{Inter}
\setmonofont[RawFeature={fallback=emojifallback}]{Hasklug Nerd Font}
\renewcommand{\familydefault}{\sfdefault}
\author{Jamie D Matthews}
\date{\today}
\title{Notes on Calculus}
\hypersetup{
 pdfauthor={Jamie D Matthews},
 pdftitle={Notes on Calculus},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.0.91 (Org mode 9.6.5)}, 
 pdflang={English}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle
\setcounter{tocdepth}{2}
\tableofcontents


\section*{Foreword}
\label{sec:org99a0504}

The word ``calculus'' comes from the Latin for ``stone'', which were used in antiquity to represent quantities for calculations---thus, calculus came to mean some system or method of calculation.
There are many \emph{caluli} in current use, including for example the lambda calculus used in computer science.
As the study of derivatives and integrals took off, this calculus became so important as to just be called ``calculus'' or ``the calculus''.

Calculus is foundational for much of mathematics, and both analytic and numerical calculus techniques are extremely important in science and engineering.

\section*{Differential Calculus}
\label{sec:org68332ee}
\subsection*{Derivatives}
\label{sec:orgf803770}

\textbf{Definition}: Derivative of a function \(f(x)\) \emph{with respect to \(x\)}

\[  \frac{df}{dx} = \lim_{h \to 0} \frac{f(x + h) - f(x)}{h} \]

A function is differentiable at a point if the limit exists at that point \emph{from both sides}.
Therefore for example, \(f(x) = |x|\)  is not differentiable at \(x = 0\)  as the limits from left and right differ (-1 and 1).

\textbf{Notation}: 

\[ \frac{d}{dx}f(x) = \frac{df}{dx} = f'(x) \]

for higher (nested) derivatives:

\[ \frac{d}{dx}\left(\frac{d}{dx} \left( f(x) \right) \right) = f''(x) = \frac{d^{2}f}{dx^{2}}\]

importantly, the prime notation \(f'\) refers to the derivative of its argument:

\[ f'(2x) = \frac{df}{d(2x)} \]

\subsection*{Big and small \(o\) notation}
\label{sec:org0526036}

Big and small \(O\) notation describes the limiting behaviour of functions as the argument tends to some value, usually zero or infinity.
This is the same as the big \(O\) notation used in computer science as a measure of algorithmic efficiency in the sense of how fast runtime or space requirements grow as the input size grows.
This notation is introduced here as it is frequently used in calculus.

\subsubsection*{Small}
\label{sec:orgc20eb5a}

\[ f(x) = o(g(x)) \text{ as } x \to x_{0} \text{ if } \lim_{x \to x_{0}} \frac{f(x)}{g(x)} = 0 \]

Read: ``\(f(x)\) is small-o of \(g(x)\)''.
Intuitively, \(f(x)\) is \emph{much smaller than}  \(g(x)\).

Note the notation using the equals is an abuse of notation, but this is what is commonly in use.
Probably a better notation would have been to use set membership.

\subsubsection*{Big}
\label{sec:orgbb4ff75}

\[ f(x) = O(g(x)) \text{ as } x \to x_{0} \text{ if } \lim_{x \to x_{0}} \frac{f(x)}{g(x)} \text{ is bounded}\]

Read: ``\(f(x)\) is big-O of \(g(x)\)''.
Intuitively, \(f(x)\) is ``about as big'' as \(g(x)\).

This is used in computer science to say ``this algorithm's time or space requirements grows with the input size like \(O(n \log{n})\)'' for example.

\subsubsection*{Examples}
\label{sec:org39b13dd}

\[ x = o(\sqrt{x}) \text{ as } x \to 0\]

Here we are approaching 0 from the right.

\[ \sqrt{x} = o(x) \text{ as } x \to \infty\]

\[ \sin{2x} = O(x) \text{ as } x \to 0 \]

This is because \(sin{\theta} \approx \theta\) for small \(\theta\).

\[ \sin{2x} = O(1) \text{ as } x \to \infty \]

This is true despite the limit not existing.

\subsubsection*{{\bfseries\sffamily TODO} Use in calculus}
\label{sec:org9ef2016}

If we want to ignore all second-order terms in an expression, we can just write the first-order terms and append \(+ O(x^{2})\).

\textbf{Proposition}.

\[ f(x_{0} + h) = f(x_{0}) + f'(x_{0})h + o(h)\]

\emph{Proof}.

By the definition of the derivative and small-O notation.

\subsection*{Methods of Differentiation}
\label{sec:orgffb7103}

Many problems involving derivatives can be solved by applying some of a small set of rules which have been accumulated by mathematics over the years.

\subsubsection*{{\bfseries\sffamily TODO} Chain Rule}
\label{sec:orge41697f}

\textbf{Theorem.}

Given \(f(x) = g(h(x))\), then

\[ \frac{df}{dx} = \frac{dg}{dh} \frac{dh}{dx} \]

\emph{Proof}. Assuming that \(\frac{dh}{dx}\) exists and therefore is finite:

\subsubsection*{{\bfseries\sffamily TODO} Product Rule}
\label{sec:org97bc374}

\textbf{Theorem}.

Given \(f(x) = u(x) v(x)\), then

\[ \frac{df}{dx} = \frac{du}{dx} v(x) + \frac{dv}{dx} u(x) \]

\emph{Proof}.

\subsubsection*{Leibniz's Rule}
\label{sec:org58c5ec4}
\subsection*{Taylor's Theorem}
\label{sec:org772a315}
\subsection*{L'Hopital's Rule}
\label{sec:orgd373523}
\section*{Integration}
\label{sec:orgacb77f8}
\subsection*{Integrating}
\label{sec:orgfa72484}
\subsection*{Fundamental Theorem of Calculus}
\label{sec:org17ac947}
\subsection*{Methods of Integration}
\label{sec:org96511ba}
\subsubsection*{Integration by Substitution}
\label{sec:orgada2d64}
\subsubsection*{Integration by Parts}
\label{sec:org57fb4a4}
\section*{Partial Differentiation}
\label{sec:orgaef5674}
\subsection*{Partial Derivatives}
\label{sec:org79df358}
\subsection*{}
\label{sec:org67e9891}
\section*{First Order Differential Equations}
\label{sec:org8dc5cd0}

\section*{Exponential Differential Equations}
\label{sec:org92ec45c}

Comments from Liz's Ben:

\begin{verbatim}
Prove with chain rule and fundamental theorem of calculus

dn/dt = n

is exponential

Lebesgue and Reimann integration helpful formalisms, as is measure theory.

Treating dy/dx as a fraction comes from the chain rule.
\end{verbatim}

\section*{Sources}
\label{sec:org4e7168f}

\begin{itemize}
\item \url{https://dec41.user.srcf.net/notes/} (Notes from a student of Cambridge's Math Tripos)
\end{itemize}
\end{document}