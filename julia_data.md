---
title: Data with Julia Cheatsheet

---

```julia
using CSV, DataFrames
```

# Reading Data

### Happy Path
```julia
df = DataFrame(CSV.File("path/to/df.tsv")) # autodetects delim 
```

### No Header
```julia
# header = false for no header at all
df = DataFrame(CSV.File("path/to/df.tsv"; header=[:col1, :col2, :col3])) 
```

### Weird Delimiter
```julia
df = DataFrame(CSV.File("path/to/df.tsv"; delim=":~:"))
```

### Normalise Column Names
```julia
# Symbol("col 1") -> :col_1
df = DataFrame(CSV.File("path/to/df.tsv"; normalizenames = true))
```

### Skipping Lines
```julia
df = DataFrame(CSV.File("path/to/df.tsv"; limit = 3)) # just first 3
df = DataFrame(CSV.File("path/to/df.tsv"; skipto = 4, footerskip = 4)) # skip at start and end
```

### Transposed Data
```julia
df = DataFrame(CSV.File("path/to/df.tsv"; transpose = true)) # input like: col1, 0.02, 0.03\n col2, 0.03, 0.05
```

# Extracting Data

### Access a Column
```julia
# reference the column
df.col1 # or
getproperty(df, :col1) # or
df[!, :col1]

# copy the column
df[:, :col1]

# remember
df.x === df[!, :x] # true
df.x === df[:, :x] # false
df.x ==  df[:, :x] # true
```

# Mutating Data

### Add a Column
```julia
df.new_col = 1:50
```

### Rename Columns
```julia
rename!(df, :col1 => :new1, :col2 => :new2)
```

### Sort Table
```julia
sort(df, :x) # sort on x
sort(df, [:x, :y]) # sort on x then y
sort(df, [:x, :y], rev=true) # reverse
sort(df, [:x, order(:y, rev=true)]) # mix orders
sort!(df, :x) # mutate instead of return new df
```

### Subset of Columns
```julia
select(df, :keep1, :keep2) # columns as args
select(df, [:keep1, :keep2]) # or an array
select(df, Not([:drop1, :drop2])) # all but
```

### Deriving New Colums from Existing
```julia
df.y = df.x .^2 # column y is column x squared
```

### Randomise a Column
```julia
using Random
shuffle!(df.col1) # breaks relation with col2, col3...
```

# Writing Data

### Tab-Separated (`.tsv`)
```julia
CSV.write("intended/path/df.tsv", df; delim = '\t')
```

### Comma-Separated (`.csv`)
```julia
CSV.write("intended/path/df.csv", df)
```

