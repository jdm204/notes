* Summary
  :PROPERTIES:
  :CUSTOM_ID: summary
  :END:

* Resources
  :PROPERTIES:
  :CUSTOM_ID: resources
  :END:

- Machine Learning I Dmitry Kobak University of Tubingen

* Feature Selection
  :PROPERTIES:
  :CUSTOM_ID: feature-selection
  :END:

Selecting a subset of data features in order to reduce complexity of the
model or to make a dataset more computationally tractable.

* Feature Extraction
  :PROPERTIES:
  :CUSTOM_ID: feature-extraction
  :END:

Feature extraction builds a derived dataset from an initial one with the
derived features intending to be informative and non-redundant.

* Principal Component Analysis (PCA)
  :PROPERTIES:
  :CUSTOM_ID: principal-component-analysis-pca
  :END:

PCA is an /orthoganal linear transformation/ of data to a new coordinate
system such that the greatest variance by some scalar projection of the
data comes to lie on the first coordinate, which is called the first
principal component. The second greatest variance is projected onto the
second principal component and so on.

PCA combines highly correlated variables together to form a smaller
number of artificial variables, called principal components.

PCA can be thought of as fitting an /n/-dimensional ellipsoid to data
with /n/ features - each axis of the ellipsoid then represents a
principal component.

** Mathematics
   :PROPERTIES:
   :CUSTOM_ID: mathematics
   :END:

Mathematically, principal components are *eigenvectors of the data's
covariance matrix*. Because the covariance matrix is symmetric, the
eigenvectors are orthogonal. The principal components (eigenvectors)
correspond to the direction with the greatest variation in origional
/n/-dimensional space.

Each eigenvector has a corresponding scalar eigenvalue, which indicates
the amount of variation captured by its eigenvector.

** Steps
   :PROPERTIES:
   :CUSTOM_ID: steps
   :END:

- Normalise the input variables (predictors) by subtracting the mean
  from each data point

  - Each column (variable) now has a mean of 0

** How many PCs
   :PROPERTIES:
   :CUSTOM_ID: how-many-pcs
   :END:

Use a scree curve and look for an 'elbow' (bend) in the curve.

Kaiser's Rule: select PCs which have eigenvalues >1. Lower than one
means a PC carries less information than a single input variable.

Or pick PCs until /x/ variation is explained, where /x/ might be 80%,
95% etc depending on the application.

* Non-negative matrix factorisation (NMF)
  :PROPERTIES:
  :CUSTOM_ID: non-negative-matrix-factorisation-nmf
  :END:

* Linear Discriminant Analysis (LDA) / Generalised Discriminant Analysis
(GDA)
  :PROPERTIES:
  :CUSTOM_ID: linear-discriminant-analysis-lda-generalised-discriminant-analysis-gda
  :END:

* T-Distributed Stochastic Neighbor Embedding (t-SNE)
  :PROPERTIES:
  :CUSTOM_ID: t-distributed-stochastic-neighbor-embedding-t-sne
  :END:

- does not preserve distances or densities well

  - thus not recommended for clustering or outlier detection

* Uniform Manifold Approximation and Projection (UMAP)
  :PROPERTIES:
  :CUSTOM_ID: uniform-manifold-approximation-and-projection-umap
  :END:

* PHASE (2021)
  :PROPERTIES:
  :CUSTOM_ID: phase-2021
  :END:
