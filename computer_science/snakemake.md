---
title: Reproducible Computing with Snakemake
author: JD Matthews
---

# OS Should be Reproducible

Containers good enough for this.

# Software Versioning

Conda envs for each tool.

# Workflow Changes

Workflow itself under version control, commit IDs as workflow
versions.
