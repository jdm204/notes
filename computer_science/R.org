* R language
** Names and Atomic Vectors
  :PROPERTIES:
  :CUSTOM_ID: names-and-atomic-vectors
  :END:

A syntactic (valid) name must consist of letters, digits, =.= and =_=
but can't begin with =_= or a digit, can't be a =Reserved= word - unless
quoted with backticks.

#+BEGIN_SRC R
  x <- 4
  typeof(x) ## double

  y <- c(4, 4, 5)
  typeof(y) ## double

  y[3] ## 5
  typeof(y[3]) ## double

  z <- 4L
  typeof(x) ## integer

  a <- "foo"
  typeof(x) ## character

  b <- TRUE
  typeof(a) ## logical
#+END_SRC

The name =x= is bound to the object (value) =4=, and has type =double=,
which means a vector of floats, in this case with one element. R has no
type for a single value; a single float is a =double= vector with one
element.

Suffixing a number literal with "L" gives an =integer= vector, and there
are =character= vectors and =logical= vectors for strings and truth
values. These are the four common atomic vector types, there are also
=raw= for raw bytes and =complex= for complex numbers.

Atomic vectors can only contain elements of the above 6 types - they
cannot contain themselves for example - and they cannot be heterogeneous
(eg can't mix integer and double values), unlike the other kind of
vector - a list.

As a result of the atomicity of vectors, the concatenation function will
"flatten" vectors:

#+BEGIN_SRC R
  c(c(1, 2), c(3, 4), c(5, 6)) ## 1 2 3 4 5 6
#+END_SRC

=NULL= can be though of as a generic zero-length vector.

*** Copy-on-Modification
   :PROPERTIES:
   :CUSTOM_ID: copy-on-modification
   :END:

#+BEGIN_SRC R
  library(lobstr)
  x <- 1:3
  y <- x
  obj_addr(x) == obj_addr(y) ## TRUE
  x[2] <- 4
  obj_addr(x) == obj_addr(y) ## FALSE
#+END_SRC

Use =tracemem(obj)= to be notified when =obj= is copied.

** Lists
  :PROPERTIES:
  :CUSTOM_ID: lists
  :END:

Storing heterogenous data is the job of =list=s.

#+BEGIN_SRC R
  x <- list(1, 2, "miss a few", 99, 100)
  x[[3]] ## "miss a few"
#+END_SRC

Lists are essentially vectors of pointers, so in a sense, lists are
homogeneous.

#+BEGIN_SRC R
  c(list(1, 2, 3), list(4, 5, 6)) ## same as list(1, 2, 3, 4, 5, 6)
  list(list(1, 2, 3), list(4, 5, 6)) ## now a list of two sublists
#+END_SRC

** Types with Attributes
  :PROPERTIES:
  :CUSTOM_ID: types-with-attributes
  :END:

Vectors can have a named list of arbitrary data attached as attributes.

#+BEGIN_SRC R
  x <- 4
  attr(x, "foo") <- "bar"
  attr(x, "foo") ## "bar"
  attributes(x) ## $foo: "bar"
#+END_SRC

Special attributes can affect behaviour:

#+BEGIN_SRC R
  x <- 1:6
  ## 1 2 3 4 5 6
  attr(x, "dim") <- c(2, 3) ## give vector dimensions
  ## 1 3 5
  ## 2 4 6
  dim(x) <- NULL ## back to single dimensional (NULL dimensional really)
  attr(x, "names") <- c("a", "b", "c", "d", "e", "f")
  ## a b c d e f
  ## 1 2 3 4 5 6
#+END_SRC

** S3 Objects
  :PROPERTIES:
  :CUSTOM_ID: s3-objects
  :END:

Having a =class= attribute turns an object into an S3 object.

- =factor=
- =Date=
- =data.frame=
- =Tibble=

*** =data.frame=
   :PROPERTIES:
   :CUSTOM_ID: data.frame
   :END:

Attributes: =names=, =class=, =row.names=.

=ncol()= == =length()=, =nrow()=, =colnames()= == =names()=,
=rownames()=

Subsetting with =df$col= for columns

** Subsetting
  :PROPERTIES:
  :CUSTOM_ID: subsetting
  :END:

Subsetting operators: =$=, =[=, =[[= work differently on different
vector types.

*** Subsetting Atomic Vectors
   :PROPERTIES:
   :CUSTOM_ID: subsetting-atomic-vectors
   :END:

By natural numbers, negative integers, logical vectors.

#+BEGIN_SRC R
  x <- c(1:10)
  y <- x[x > 5] ## 6 7 8 9 10

  names(y) <- letters[6:10]
  y[c('f', 'j')] ## 6 10
#+END_SRC

Don't index with factors, they coerce to integers.

*** Subsetting Lists
   :PROPERTIES:
   :CUSTOM_ID: subsetting-lists
   :END:

#+BEGIN_SRC R
  x <- list(1, "two", TRUE)
  typeof(x[2]) ## list of length 1 containing character vector
  typeof(x[[2]]) ## character vector
#+END_SRC

*** Subsetting Matrices
   :PROPERTIES:
   :CUSTOM_ID: subsetting-matrices
   :END:

** S4 Objects
  :PROPERTIES:
  :CUSTOM_ID: s4-objects
  :END:

** Reference Classes (S5)
  :PROPERTIES:
  :CUSTOM_ID: reference-classes-s5
  :END:

** S6 Objects
  :PROPERTIES:
  :CUSTOM_ID: s6-objects
  :END:

** Interrogating R
  :PROPERTIES:
  :CUSTOM_ID: interrogating-r
  :END:

=lobstr= =pryr=?

=tracemem= =gc=

* Data with R

** Manipulating Data
  :PROPERTIES:
  :CUSTOM_ID: manipulating-data
  :END:

*** Strings
   :PROPERTIES:
   :CUSTOM_ID: strings
   :END:

**** Slices
    :PROPERTIES:
    :CUSTOM_ID: slices
    :END:

#+BEGIN_SRC R
  str_sub("foo_bar", 5, 7) # returns bar
  str_sub("foo_bar", start = 5) # returns bar
  str_sub("foo_bar", end = 3) # returns foo
#+END_SRC

**** Splitting
    :PROPERTIES:
    :CUSTOM_ID: splitting
    :END:

#+BEGIN_SRC R
  # strsplit returns a list wrapper so shed it first
  strsplit("foo_bar_baz", "_")[[1]][3] # returns baz
#+END_SRC

**** Joining
    :PROPERTIES:
    :CUSTOM_ID: joining
    :END:

#+BEGIN_SRC R
  paste0("b", "a", "r") # returns bar
#+END_SRC

**** String in String?
    :PROPERTIES:
    :CUSTOM_ID: string-in-string
    :END:

#+BEGIN_SRC R
  str_detect("foobar", "oo") # true
#+END_SRC

*** Map
   :PROPERTIES:
   :CUSTOM_ID: map
   :END:

#+BEGIN_SRC R
  map <- function (func, list) { sapply(list, func) }
  map(sqrt, 1:10)
#+END_SRC

** Plotting
  :PROPERTIES:
  :CUSTOM_ID: plotting
  :END:

#+BEGIN_SRC R
  library(tidyverse) # ggplot plotting, dplyr filter select etc, readr read_tsv
  library(patchwork) # composing subplots
  library(ggrepel)   # labelling points
  library(cowplot)   # includes minimalistic themes (theme_half_open)
#+END_SRC

*** Arranging subplots
   :PROPERTIES:
   :CUSTOM_ID: arranging-subplots
   :END:

- =patchwork=
- =cowplot=
- =gridExtra=

**** Subplots with shared axes
    :PROPERTIES:
    :CUSTOM_ID: subplots-with-shared-axes
    :END:

Axis sharing is not automatic with =patchwork= but lining up the axes
is, so to complete the effect the duplicate axes (+labels) need to be
removed.

#+BEGIN_SRC R
  squelch_y <- function(x) { theme(axis.text.y = element_blank(),
                                   axis.title.y = element_blank(),
                                   axis.ticks.y = element_blank()) }
#+END_SRC

If we have two plots =p1= and =p2= which share a y axis, we can arrange
them like:

#+BEGIN_SRC R
  p1 + p2 + squelch_y() # axis on left
  p1 + squelch_y() + p2 # axis in middle
#+END_SRC

* R Stats

** Power Calculations

#+begin_src R :session
  power.t.test(n=10, sd=1, power=0.99, sig.level=0.05)
#+end_src

* Dates

** Parsing Org Mode Datetimes

Using =lubridate=:

#+begin_src R :session
  org_datetime = "[2023-11-18 Sat 21:06]"
  datetime = lubridate::parse_date_time(org_datetime, "[%Y-%m-%d %* %H:%M]")
#+end_src

** Getting an integer number of days

For a vector =date= of dates, including the starting one:

#+begin_src R :session
  days_from_start = difftime(date, min(date), units="days") |> as.numeric()
#+end_src
