# Names and Atomic Vectors

A syntactic (valid) name must consist of letters, digits, `.` and `_`
but can’t begin with `_` or a digit, can't be a `Reserved` word -
unless quoted with backticks.

```R
x <- 4
typeof(x) ## double

y <- c(4, 4, 5)
typeof(y) ## double

y[3] ## 5
typeof(y[3]) ## double

z <- 4L
typeof(x) ## integer

a <- "foo"
typeof(x) ## character

b <- TRUE
typeof(a) ## logical
```

The name `x` is bound to the object (value) `4`, and has type
`double`, which means a vector of floats, in this case with one
element. R has no type for a single value; a single float is a
`double` vector with one element.

Suffixing a number literal with "L" gives an `integer` vector, and
there are `character` vectors and `logical` vectors for strings and
truth values. These are the four common atomic vector types, there are
also `raw` for raw bytes and `complex` for complex numbers.

Atomic vectors can only contain elements of the above 6 types - they
cannot contain themselves for example - and they cannot be
heterogeneous (eg can't mix integer and double values), unlike the
other kind of vector - a list.

As a result of the atomicity of vectors, the concatenation function
will "flatten" vectors:

```R
c(c(1, 2), c(3, 4), c(5, 6)) ## 1 2 3 4 5 6
```

`NULL` can be though of as a generic zero-length vector.

## Copy-on-Modification

```R
library(lobstr)
x <- 1:3
y <- x
obj_addr(x) == obj_addr(y) ## TRUE
x[2] <- 4
obj_addr(x) == obj_addr(y) ## FALSE
```

Use `tracemem(obj)` to be notified when `obj` is copied.

# Lists

Storing heterogenous data is the job of `list`s.

```R
x <- list(1, 2, "miss a few", 99, 100)
x[[3]] ## "miss a few"
```

Lists are essentially vectors of pointers, so in a sense, lists are
homogeneous.

```R
c(list(1, 2, 3), list(4, 5, 6)) ## same as list(1, 2, 3, 4, 5, 6)
list(list(1, 2, 3), list(4, 5, 6)) ## now a list of two sublists
```

# Types with Attributes

Vectors can have a named list of arbitrary data attached as
attributes.

```R
x <- 4
attr(x, "foo") <- "bar"
attr(x, "foo") ## "bar"
attributes(x) ## $foo: "bar"
```

Special attributes can affect behaviour:

```R
x <- 1:6
## 1 2 3 4 5 6
attr(x, "dim") <- c(2, 3) ## give vector dimensions
## 1 3 5
## 2 4 6
dim(x) <- NULL ## back to single dimensional (NULL dimensional really)
attr(x, "names") <- c("a", "b", "c", "d", "e", "f")
## a b c d e f
## 1 2 3 4 5 6
```


# S3 Objects

Having a `class` attribute turns an object into an S3 object.

- `factor`
- `Date`
- `data.frame`
- `Tibble`

## `data.frame`

Attributes: `names`, `class`, `row.names`.

`ncol()` == `length()`, `nrow()`, `colnames()` == `names()`, `rownames()`

Subsetting with `df$col` for columns

# Subsetting

Subsetting operators: `$`, `[`, `[[` work differently on different
vector types.

## Subsetting Atomic Vectors

By natural numbers, negative integers, logical vectors.

```R
x <- c(1:10)
y <- x[x > 5] ## 6 7 8 9 10

names(y) <- letters[6:10]
y[c('f', 'j')] ## 6 10
```

Don't index with factors, they coerce to integers.

## Subsetting Lists

```R
x <- list(1, "two", TRUE)
typeof(x[2]) ## list of length 1 containing character vector
typeof(x[[2]]) ## character vector
```

## Subsetting Matrices


# Functions

## Replacement Functions

Assigning to a function calls a special 'replacement' function ending in `<-`:

```R
x <- c(1, 2, 3)
names(x) <- c("one", "two", "three")
names(x) ## one two three
`names<-`(x, c("foo", "bar", "bax"))
names(x) ## foo bar baz
```

## Special Forms

- `(`
- `[`
- `function`
- `for`
- etc

# Environments

# Conditions

- `message()`
- `warning()`
- etc

# S4 Objects

# Reference Classes (S5)

# S6 Objects

# Interrogating R

`lobstr` `pryr`?

`tracemem` `gc`

# Tips etc

```{R}
# set the center of bars for histograms
df %>% ggplot(aes(x)) + geom_histogram(center=2.5)
```

- `scale_x_continuous(expand=c(0, 0)) # don't expand axes

- colorspace package for scale_fill_continuous_qualitative() etc
- ggthemes scale_color_colorblind()
- increase font size in themes like theme_gray(14)

```{R, message=F, warning=F}
# reorder
penguins %>%
  count(species) %>%
  mutate(species = fct_reorder(species, n)) %>%
  ggplot(aes(n, species)) + geom_col()
```

```{R, message=F, warning=F}
# fit models to subsets of data
lm_summary <- penguins %>%
  nest(data = -species) %>%
  mutate(
    fit = map(data, ~lm(flipper_length_mm ~ body_mass_g, data = .x)),
    glance_out = map(fit, glance)
  ) %>%
  select(species, glance_out) %>%
  unnest(cols = glance_out)

```

```{R, message=F, warning=F}
label_data <- lm_summary %>%
  mutate(
    rsqr = signif(r.squared, 2),  # round to 2 significant digits
    pval = signif(p.value, 2),
    label = glue("R^2 = {rsqr}, P = {pval}"),
    body_mass_g = 6400, flipper_length_mm = 175 # label position in plot
  ) %>%
  select(species, label, body_mass_g, flipper_length_mm)

ggplot(penguins, aes(body_mass_g, flipper_length_mm)) + geom_point() +
  geom_text(
    data = label_data, aes(label = label),
    size = 10/.pt, hjust = 1  # 10pt, right-justified
  ) +
  geom_smooth(method = "lm", se = FALSE) + facet_wrap(vars(species))
```
