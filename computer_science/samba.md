# Recursively copy a directory to SMB

```bash
smbclient -U BLUE/jdm204 //rfs.uis.private.cam.ac.uk/rfs-sdt36-turner-lab/
# enter Raven password
recurse ON
prompt OFF
mkdir path\on\smb\myproj01
cd path\on\smb\myproj01
lcd local/path/myproj01
mput *
```

Note that if I wanted to move a dir tree 'foo' to remote, this
procedure requires to `mkdir foo` on the remote, `lcd foo` on the
local, `cd foo` on the remote, and then `mput *`.

```bash
smbclient -U BLUE/jdm204 //rfs.uis.private.cam.ac.uk/rfs-sdt36-turner-lab/
!ls # local commands prefixed with '!'
```
