---
title: Data in R
author: Jamie D Matthews
---

# Manipulating Data

## Strings

### Slices

```R
str_sub("foo_bar", 5, 7) # returns bar
str_sub("foo_bar", start = 5) # returns bar
str_sub("foo_bar", end = 3) # returns foo
```

### Splitting

```R
# strsplit returns a list wrapper so shed it first
strsplit("foo_bar_baz", "_")[[1]][3] # returns baz
```

### Joining 

```R
paste0("b", "a", "r") # returns bar
```

### String in String?

```R
str_detect("foobar", "oo") # true
```

## Map

```R
map <- function (func, list) { sapply(list, func) }
map(sqrt, 1:10)
```

# Manipulating Tables

- `data.table` is the fastest tables package
- `tibble`/`dplyr`/`readr` has the most readable syntax
- `dtplyr` can translate `dplyr` syntax to `data.table`

`library(data.table); library(tidyverse)`

## Manual Table Creation

```R
tibble(x = runif(10), y = x * 2, z = z)
```

## Filtering

```R
dt[] # todo
```

# Plotting

```R
library(tidyverse) # ggplot plotting, dplyr filter select etc, readr read_tsv
library(patchwork) # composing subplots
library(ggrepel)   # labelling points
library(cowplot)   # includes minimalistic themes (theme_half_open)
```

## Arranging subplots

- `patchwork`
- `cowplot`
- `gridExtra`

### Subplots with shared axes

Axis sharing is not automatic with `patchwork` but lining up the axes
is, so to complete the effect the duplicate axes (+labels) need to be
removed.

```R
squelch_y <- function(x) { theme(axis.text.y = element_blank(),
                                 axis.title.y = element_blank(),
								 axis.ticks.y = element_blank()) }
```

If we have two plots `p1` and `p2` which share a y axis, we can
arrange them like:

```R
p1 + p2 + squelch_y() # axis on left
p1 + squelch_y() + p2 # axis in middle
```
