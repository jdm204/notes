#set document(title: "Intratumour Heterogeneity", author: "Jamie D Matthews")
#set text(lang: "en")
#outline()
#set heading(numbering: "1.")
#heading(level: 1, outlined: false, numbering: none)[Cell autonomous vs non-autonomous ITH] #label("org92bb7e6")
Cell-autonomous ITH:

#list(list.item[genetic (SNVs, SVs, CNVs)])#list(list.item[epigenetic (methylation, histone modifications)])#list(list.item[non- genetic/epigenetic gene expression variation

Cell non-autonomous ITH:

#list(list.item[vasculature/hypoxia])#list(list.item[immune infiltration])#list(list.item[non-immune stromal cells])#list(list.item[pressure, shear stress and other physical variables])#list(list.item[chemical species (mutagens, nutrients)])])

This distinction is largely artificial as cell-autonomous factors can influence cell non-autonomous factors and vice versa, for example: part of a tumour is exposed to a mutagen, which leads to oncogene activation
#heading(level: 1, outlined: false, numbering: none)[Hierarchial models] #label("org4f2567d")
#heading(level: 2, outlined: false, numbering: none)[Evidence for Hierarchial organisation in cancer] #label("orgc6b318c")
#list(list.item[single transplanted multipotent malignant cells reproduce functional hierarchy of teratocarcinoma(??, ????)])
#heading(level: 1, outlined: false, numbering: none)[Genetic ITH] #label("org9f42b27")
#heading(level: 2, outlined: false, numbering: none)[Cancer cell fractions, variant allele frequencies] #label("org49dce0b")
#heading(level: 3, outlined: false, numbering: none)[What is a Cancer Cell Fraction?] #label("org1e68adb")
The CCF of a variant is the fraction of cancer cells which carry the variant out of all of the cancer cells in the population. It is therefore a measure of intratumour heterogeneity of the variant---clonal variants carried by all cancer cells by definition have a CCF of $1$, while subclonal variants have a CCF $<1$.

An example of when a CCF might be important is when using a targeted therapy; if the therapy targets cells with a particular variant, if the CCF of that variant is less than one, then the cells without that variant can go on to survive and reconstitute the tumour.
#heading(level: 3, outlined: false, numbering: none)[What is Variant Allele Frequency?] #label("orge9a5251")
The VAF is a property of a locus, particularly relating to a specific variant allele at that locus, so first some definitions:

#figure([#table(columns: 2, [Term],[Definition],
[locus],[genetic position on a chromosome],
[reference (sequence)],[a potentially arbitrary sequence representing an organism],
[],[against which sequences in the population are compared to],
[allele],[a version of a sequence at a locus],
[variant],[an allele which is not reference],
)]) #label("org75f29e0")

It's important to note that the reference allele is not special---in particular it is not generally the most common allele, longest allele, or a 'healthy' allele, it is just the one that corresponds to the reference. The #raw("hg38") (#raw("GRCh38")) human reference genome for example is mostly (~70%) derived from one anonymous male donor! 

A #text(weight: "bold", [variant allele frequency]) is the proportion of a variant allele at a locus out of all the other alleles at that locus, in the population of alleles we are considering.

If we do some targeted sequencing of a locus in a DNA sample, we can collect all the sequences and align them, and then count the numbers of each allele at the locus of interest, like in the following example. The middle column (asterisk) is our locus of interest.

As you can see the reference allele is #raw("C"), and we have detected a variant allele #raw("A") at the locus. We have 3 counts of the #raw("A") allele and 2 counts of the #raw("C") allele. We can calculate an #text(weight: "bold", [empirical VAF]) (measured frequency of the alleles in our sequencing sample) as $\frac{C_{v}}{C_{r} + C_{v}}$ where $C_{v}$ is the count of the variant allele and $C_{r}$ is the count of the reference allele. In this example it would be $\frac{3}{5}$ or 0.6.

Importantly, the empirical VAF of a sample is not in general equal to the actual VAF of a population due to sampling variability---if the organism in our example locus above was diploid at that locus (two copies), and our detection of both #raw("C") and #raw("A") alleles in the sample was not in error, then the VAF in the population is 0.5. This is quite different from the VAF we measured, mainly because we had poor depth (few reads) at the locus, which means that the binomial sampling variability is high. With an odd number of reads at a diploid locus the empirical VAF can never equal the actual VAF because we can't get a count of 2.5 variant alleles!

In general, a locus might be #text(weight: "bold", [multiallelic]), meaning there is more than one variant alleles, each having its own VAF. Simple single-nucleotide changes can only have three variant alleles, but considering insertions and deletions (indels) the number of possible alleles is arbitrary. In general then, we can index all the alleles of a locus $1 \dots N$ and talk about the i#super[th] variant out of all the N variants in the population. The VAF of the i#super[th] allele is then



where $C_{i}$ is the count of the i#super[th] allele.

Many things affect the VAF, such as the proportion of normal cells in the sample, overall ploidy, copy number alterations etc. We generally want to use the VAF, which is easily measureable, to derive good estimates of other paramaters or to infer biology, such as the extent intratumour heterogeneity and the number of detectable subclones under selection for example.
#heading(level: 4, outlined: false, numbering: none)[Other allele frequencies] #label("org2faec05")
The VAF is rather reference-centric. We can also talk about major or minor allele frequencies, the frequencies of the most common and second most common alleles respectively. We can also talk about a mutant allele frequency, where we are considering the allele to differ from some particular sequence of interest rather than a reference.
#heading(level: 3, outlined: false, numbering: none)[Relating VAF and CCF] #label("org14ce253")
Depending on the extent of simplifying assumptions we are willing to make, relating VAF and CCF may be more or less simple. In the following, we refer to the VAF of a particular variant as $f$ and the corresponding CCF as $F$.
When we want to refer to read counts, we say $r_{v}$ for the number of reads reporting the variant allele and $r_{r}$ for the number of reads reporting the reference allele, with $r = r_{v} + r_{r}$ meaning the depth at
 the locus.
#heading(level: 4, outlined: false, numbering: none)[In the literature] #label("org565e7c1")
#heading(level: 5, outlined: false, numbering: none)[Nik-Zainal et al (??, a)] #label("orged24c67")
// todo: org-typst-latex-environment
#heading(level: 4, outlined: false, numbering: none)[Hypothetical populations] #label("orge8565de")
#figure([#raw(block: true, lang: "julia", "using Test
using Chain")]) #label("orgda9b80e")

We make a Julia data structure to hold the data we want to associate to each hypothetical tumour sample.

#figure([#raw(block: true, lang: "julia", "struct LocusPopulation
    πₙ::BigFloat
    πᵣ::BigFloat
    πᵥ::BigFloat
    ϕ::BigFloat
    ρ::BigFloat
    F::BigFloat
    f::BigFloat
    LocusPopulation(; πₙ, πᵣ, πᵥ, ϕ, ρ, F, f) = new(πₙ, πᵣ, πᵥ, ϕ, ρ, F, f)
end")]) #label("org005e2c8")

We can then design a simple format which specifies a hypothetical tumour cell population. We use #raw("|") for a "reference" allele and #raw("*") for a variant allele, and a line for each population we consider. We know the VAF, CCF, purity etc for this population, so we can use them to test relationships between VAF and CCF.

#figure([#raw(block: true, lang: "julia", "poptest = \u{22}\u{22}\u{22}
Normal cells          : (||), (||)
Cancer without variant: (|), (|), (|)
Cancer with variant   : (**|), (**|), (**|), (**|)
\u{22}\u{22}\u{22}")]) #label("org5dc759b")

Then we need a function to parse a LocusPopulation from our diagram stored as a string:

#figure([#raw(block: true, lang: "julia", "\u{22}parse a LocusPopulation from a string diagram\u{22}
function Base.parse(LocusPopulation, str)
    \u{22}counts | as ref and * as alt in a string and returns the counts as a tuple\u{22}
    function count_alleles(cell)::Tuple{Int, Int}
        (count('|', cell), count('*', cell))
    end
    counts = @chain str begin
        strip
        split('\n')
        map(line -> replace(line, r\u{22}^.*:\u{22} => \u{22}\u{22}), _)
        map(split ∘ strip, _)
        @aside ncells = map(length, _)
        map(line -> count_alleles.(line), _)
        map(line -> reduce(.+, line, init=(0,0)), _)
    end

    counts[1][2] + counts[2][2] == 0 ||
        throw(\u{22}Normal cells and non variant cells can't have the variant\u{22})

    pi_n = counts[1][1] / ncells[1]
    pi_r = counts[2][1] / ncells[2]

    LocusPopulation(πₙ = isnan(pi_n) ? 0 : pi_n,
                    πᵣ = isnan(pi_r) ? 0 : pi_r,
                    πᵥ = sum(counts[3]) / ncells[3],
                    ϕ = counts[3][2] / ncells[3],
                    ρ = (ncells[2] + ncells[3]) / sum(ncells),
                    F = ncells[3] / (ncells[2] + ncells[3]),
                    f = counts[3][2] / sum(sum.(counts)))
end")]) #label("org95a6f2a")

And a sanity check test to make sure that the function returns values that agree with manual counting of the diagram:

#figure([#raw(block: true, lang: "julia", "expected = LocusPopulation(πₙ = 2, πᵣ = 1, πᵥ = 3,
                           ϕ = 2, ρ = 7/9,
                           f = 8/19, F = 4/7)
actual = parse(LocusPopulation, poptest)

@test string(actual) == string(expected)")]) #label("org5c3f6b7")

Test Passed
  Expression: string(actual) #raw("= string(expected)
   Evaluated: \u{22}LocusPopulation(2.0, 1.0, 3.0, 2.0, 0.77777777777777779011358916250173933804035186767578125, 0.5714285714285713968507707249955274164676666259765625, 0.421052631578947345047936323680914938449859619140625)\u{22} =") "LocusPopulation(2.0, 1.0, 3.0, 2.0, 0.77777777777777779011358916250173933804035186767578125, 0.5714285714285713968507707249955274164676666259765625, 0.421052631578947345047936323680914938449859619140625)"

We wrapped the structs in string calls because when doing #raw("==") on structs, members are compared by #raw("===") which is false for mutable objects such as #raw(block: false, "\u{22}foo\u{22} == \u{22}bar\u{22}") (false).
#heading(level: 4, outlined: false, numbering: none)[Models] #label("orgc22bd01")
#heading(level: 5, outlined: false, numbering: none)[Level 0: pure cancer cells] #label("orgc13d3ca")
With the following strong assumptions

#enum(enum.item(1)[all cells have ploidy $\pi$],
enum.item(2)[all variants have multiplicity 1],
enum.item(3)[no normal contamination],
enum.item(4)[all tumour cells are genetically identical ($F = 1$)],
)

then every cell is a cancer cell, and if one cell has a mutation, they all do, on an identical copy number background. Therefore, the frequency of variant alleles is the inverse of the ploidy $\pi$:

$f = \frac{F}{\pi} = \frac{1}{\pi}$

and the CCF is the product of the ploidy and VAF, which is one, excepting measurement/sampling error.

$F = \pi f = 1$

Relaxing the no normal contamination assumption, we then have to take into account the tumour purity \u{3c1}---the fraction of all cells which are cancer cells.

Since we are dealing with somatic variants we assume the normal cells will not have the variant, so they contribute to the denominator of the VAF (total alleles) but not the numerator (variant alleles). Only the cancer cells contribute to the numerator, which are the fraction $\rho$ of the total cells. Then we have

$f = \frac{\rho F}{\pi}$

so that the CCF is

$F = \frac{\pi f}{\rho} = 1$.

We can think of the equation for $f$ as like a "data generating process" for the empirical VAF, which is the result of a binomial trial with the probability of "success" being $f$, and the number of trials being the sequencing depth.
#heading(level: 5, outlined: false, numbering: none)[Level 1: simple population] #label("orgcddac47")
Previously $F$ was $1$ by definition, since all cancer cells had to be the same, so level 0 only models clonal variants in cells of known and consistent ploidy.

To consider subclonal variants, we relax assumption \u{23}4 (and we won't reintroduce assumption \u{23}3):

#enum(enum.item(1)[all cells have ploidy $\pi$],
enum.item(2)[all variants have multiplicity 1],
)

$f = \frac{F \rho}{\pi F \rho + \pi (1 - F) \rho + \pi(1 - \rho)}$

This reduces to 

$f = \frac{\rho F}{\pi}$

so

$F = \frac{\pi f}{\rho}$

Converting the equations to julia functions:

#figure([#raw(block: true, lang: "julia", "F₁(f, ρ, π) = (π*f)/ρ
f₁(F, ρ, π) = (ρ*F)/π")]) #label("org45c1341")

A hypothetical population meeting these assumptions might look like this:

#figure([#raw(block: true, lang: "julia", "pop1 = \u{22}\u{22}\u{22}
Normal cells          : (|||), (|||)
Cancer without variant: (|||), (|||)
Cancer with variant   : (*||), (*||), (*||)
\u{22}\u{22}\u{22}")]) #label("orgabc804e")

We can count up the number of each of the three populations as well as the ploidy $\pi$ to obtain a set of values to test that the relation between VAF and CCF holds as expected. We use normal ploidy because we assume that all cells have the same ploidy.

#figure([#raw(block: true, lang: "julia", "p1 = parse(LocusPopulation, pop1)

@testset \u{22}level 1\u{22} begin
    @test F₁(p1.f, p1.ρ, p1.πₙ) ≈ p1.F
    @test f₁(p1.F, p1.ρ, p1.πₙ) ≈ p1.f
end")]) #label("org665034e")

Test Summary: | Pass  Total
level 1       |    2      2
Test.DefaultTestSet("level 1", Any[], 2, false, false)
#heading(level: 5, outlined: false, numbering: none)[Level 2: cancer copy number] #label("org8bf8314")
Now we assume that the copy number of cancer cells and normal cells can differ (but the same within each population).



so



This is the equation used to calculate CCF in (??, a), whereby any calculated CCF $F \geq 0.9$ is taken as $F = 1$.

#figure([#raw(block: true, lang: "julia", "f₂(F, ρ, πₙ, πᵥ) = (F*ρ) / ((1 - ρ)*πₙ + ρ*πᵥ)
F₂(f, ρ, πₙ, πᵥ) = (f*(πₙ*(1 - ρ) + ρ*πᵥ)) / ρ")]) #label("org85f2977")

#figure([#raw(block: true, lang: "julia", "pop2 = \u{22}\u{22}\u{22}
Normal cells          : (||), (||)
Cancer without variant: (|||), (|||), (|||)
Cancer with variant   : (*||), (*||), (*||)
\u{22}\u{22}\u{22}")]) #label("orgf9373f6")

#figure([#raw(block: true, lang: "julia", "p2 = parse(LocusPopulation, pop2)

@testset \u{22}level 2\u{22} begin
    @test F₂(p2.f, p2.ρ, p2.πₙ, p2.πᵥ) ≈ p2.F
    @test f₂(p2.F, p2.ρ, p2.πₙ, p2.πᵥ) ≈ p2.f
end")]) #label("orgbe1e6a9")

Test Summary: | Pass  Total
level 2       |    2      2
Test.DefaultTestSet("level 2", Any[], 2, false, false)

We can further extend this to three populations of different copy number, normal, cancer cells with the variant, cancer cells without the variant.
#heading(level: 5, outlined: false, numbering: none)[Level 3: multiplicity] #label("orga5e1366")
A variant at a copy-altered locus would have a different multiplicity if the variant-causing mutation occured before or after the copy number change---a mutation followed by a single copy gain would have multiplicity of 1 or 2 depending on the copy-gained allele, whereas if the gain occurs first then the multiplicity will be one. We call the multiplicity $\phi$.

$f = \frac{F \rho \phi}{\pi F \rho + \pi (1 - F) \rho + \pi(1 - \rho)}$

so



if we consider the copy numbers of all populations equivalent. If we wish to continue to consider three populations of copy number state, we allow the copy number $\pi$ to vary for each population, and thus the copy number information for the whole model is represented as $(\pi_{n}, \pi_{cr}, \pi_{cv})$.

Now the denominator doesn't simplify like above:



Rearranging as before, we obtain:




#figure([#raw(block: true, lang: "julia", "f₃₂(F, ρ, ϕ, πₙ, πᵣ, πᵥ) = (F*ρ*ϕ) / (πᵥ*F*ρ + πᵣ*ρ*(1 - F) + πₙ*(1 - ρ))
F₃₂(f, ρ, ϕ, πₙ, πᵣ, πᵥ) = (f*(ρ*πᵣ + πₙ*(1 - ρ))) / (ϕ*ρ + f*ρ*(πᵣ - πᵥ))")]) #label("orga42925f")

#figure([#raw(block: true, lang: "julia", "pop3 = \u{22}\u{22}\u{22}
Normal cells          : (|||), (|||), (|||)
Cancer without variant: (||), (||), (||)
Cancer with variant   : (**||), (**||), (**||)
\u{22}\u{22}\u{22}")]) #label("org425b595")

#figure([#raw(block: true, lang: "julia", "p3 = parse(LocusPopulation, pop3)

@testset \u{22}level 3.2\u{22} begin
    @test F₃₂(p3.f, p3.ρ, p3.ϕ, p3.πₙ, p3.πᵣ, p3.πᵥ) ≈ p3.F
    @test f₃₂(p3.F, p3.ρ, p3.ϕ, p3.πₙ, p3.πᵣ, p3.πᵥ) ≈ p3.f
end")]) #label("org2902f4d")

Test Summary: | Pass  Total
level 3.2     |    2      2
Test.DefaultTestSet("level 3.2", Any[], 2, false, false)
#heading(level: 5, outlined: false, numbering: none)[Level 4: Fractional copy number] #label("orgd63e77a")
Now we imagine that copy number varies within each population (normal, reference, variant), such that we have a fractional copy number for each of these populations.

This is already covered by the equations from level 3, but we require more examples to check that this is robust.

#figure([#raw(block: true, lang: "julia", "pop4 = \u{22}\u{22}\u{22}
Normal cells          : (|||), (|||), (|||)
Cancer without variant: (||), (||), (||)
Cancer with variant   : (**||), (**|), (**|)
\u{22}\u{22}\u{22}")]) #label("orgc02fd60")

No normals:

#figure([#raw(block: true, lang: "julia", "pop5 = \u{22}\u{22}\u{22}
Normal cells          :
Cancer without variant: (||), (||), (||), (|)
Cancer with variant   : (**||), (**||), (**||)
\u{22}\u{22}\u{22}")]) #label("org147626f")

One where $F = 1$:

#figure([#raw(block: true, lang: "julia", "pop6 = \u{22}\u{22}\u{22}
Normal cells          : (|||), (|||), (|||), (||)
Cancer without variant: 
Cancer with variant   : (**||), (**||), (**||)
\u{22}\u{22}\u{22}")]) #label("org89fbefd")

Note that here we vary both copy number (cancer without variant) and multiplicity.

#figure([#raw(block: true, lang: "julia", "pop7 = \u{22}\u{22}\u{22}
Normal cells          : (|||), (|||), (|||), (||), (||)
Cancer without variant: (||), (||), (||), (|)
Cancer with variant   : (**||), (**||), (**||), (****|)
\u{22}\u{22}\u{22}")]) #label("org86e5139")

One with more substantial variation:

#figure([#raw(block: true, lang: "julia", "pop8 = \u{22}\u{22}\u{22}
Normal cells          : (|||), (|||), (|||), (||), (||), (||)
Cancer without variant: (||), (||), (||), (|), (|||||||||), (||||||||||)
Cancer with variant   : (**||), (**||), (**||), (*|||)
\u{22}\u{22}\u{22}")]) #label("orgdd3f44b")

#figure([#raw(block: true, lang: "julia", "pop9 = \u{22}\u{22}\u{22}
Normal cells          : (|||), (|||), (|||), (||), (||), (|)
Cancer without variant: (||), (||), (||), (|), (|)
Cancer with variant   : (**||), (**||), (**||), (**||), (*|||), (*|||)
\u{22}\u{22}\u{22}")]) #label("org55560fc")

Check the tests pass:

#figure([#raw(block: true, lang: "julia", "p4 = parse(LocusPopulation, pop4)
p5 = parse(LocusPopulation, pop5)
p6 = parse(LocusPopulation, pop6)
p7 = parse(LocusPopulation, pop7)
p9 = parse(LocusPopulation, pop9)

atol=1e-16

@testset \u{22}level 4\u{22} begin
    @test F₃₂(p4.f, p4.ρ, p4.ϕ, p4.πₙ, p4.πᵣ, p4.πᵥ) ≈ p4.F atol=atol
    @test f₃₂(p4.F, p4.ρ, p4.ϕ, p4.πₙ, p4.πᵣ, p4.πᵥ) ≈ p4.f atol=atol
    @test F₃₂(p5.f, p5.ρ, p5.ϕ, p5.πₙ, p5.πᵣ, p5.πᵥ) ≈ p5.F atol=atol
    @test f₃₂(p5.F, p5.ρ, p5.ϕ, p5.πₙ, p5.πᵣ, p5.πᵥ) ≈ p5.f atol=atol
    @test F₃₂(p6.f, p6.ρ, p6.ϕ, p6.πₙ, p6.πᵣ, p6.πᵥ) ≈ p6.F atol=atol
    @test f₃₂(p6.F, p6.ρ, p6.ϕ, p6.πₙ, p6.πᵣ, p6.πᵥ) ≈ p6.f atol=atol
    @test F₃₂(p7.f, p7.ρ, p7.ϕ, p7.πₙ, p7.πᵣ, p7.πᵥ) ≈ p7.F atol=atol
    @test f₃₂(p7.F, p7.ρ, p7.ϕ, p7.πₙ, p7.πᵣ, p7.πᵥ) ≈ p7.f atol=atol
    @test F₃₂(p8.f, p8.ρ, p8.ϕ, p8.πₙ, p8.πᵣ, p8.πᵥ) ≈ p8.F atol=atol
    @test f₃₂(p8.F, p8.ρ, p8.ϕ, p8.πₙ, p8.πᵣ, p8.πᵥ) ≈ p8.f atol=atol
    @test F₃₂(p9.f, p9.ρ, p9.ϕ, p9.πₙ, p9.πᵣ, p9.πᵥ) ≈ p9.F atol=atol
    @test f₃₂(p9.F, p9.ρ, p9.ϕ, p9.πₙ, p9.πᵣ, p9.πᵥ) ≈ p9.f atol=atol
end")]) #label("orgaf77836")

Test Summary: | Pass  Total
level 4       |   12     12
Test.DefaultTestSet("level 4", Any[], 12, false, false)

#figure([#raw(block: true, lang: "julia", "@test f₃₂(p6.F, p6.ρ, p6.ϕ, np, np, p6.πᵥ) ≈ p6.f atol=atol")]) #label("org678429f")

Test Failed at /tmp/babel-HVgY6i/julia-src-DUAaSM.jl:2
  Expression: ≈(f₃₂(p6.F, p6.ρ, p6.ϕ, np, np, p6.πᵥ), p6.f, atol = atol)
   Evaluated: 0.3428571428571428466750400535342380716623558229109798438683021254392252330909203 ≈ 0.260869565217391297107241143748979084193706512451171875 (atol=1.0e-16)
